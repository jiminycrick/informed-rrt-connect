# Informed RRT-Connect

This repository is the resting place of some experiments with various RRT algorithms for use in flight motion planning for NPCs in Veloren (https://gitlab.com/veloren/veloren). The code here includes implementations of regular RRT, Informed RRT, RRT-Connect, and an Informed RRT-Connect algorithm that as far as I know does not exist anywhere else.

In the interest of gameplay, speed was a primary concern. The current A\* pathfinding in Veloren can be slow. The main interest in a sampling algorithm was to avoid the straight lines that are apparent with A\* in 3D. The informed RRT connect algorithm works by sampling a point in an expanding ellipsoid sample space. RRTs are grown towards the sampled point from both the start and end points. New points are sampled and the sample space grows until the two trees connect. This was found to be faster than A\* in the general case where the sample space contains wider gaps. In tight corridors such as a maze A\* was faster. Notably the RRT algorithm was much cheaper than A\* over larger distances.

The Informed RRT-Connect algorithm was derived from [RRT-Connect Paper](http://www.kuffner.org/james/papers/kuffner_icra2000.pdf) and [Informed RRT\* Paper](https://arxiv.org/pdf/1404.2334v3.pdf)

The [kiddo](https://lib.rs/crates/kiddo) crate supplied K-d trees to speed up nearest neighbor searches.

The UI is built with [macroquad](https://lib.rs/crates/macroquad). Pressing `Space` will generate a path from a start position to the end position with a few different algorithms providing timing data. The map is found in the `map.txt` file. The `0` is the start and `1` is the end. `#` symbols represent walls/inaccessible positions. Pressing `M` will reload the map and reset timing data. `R` will reset the timing data without reloading the map. `S` will move the start into a new random location. `E` will move the end position in a similar fashion. `G` will generate a map using cellular automata. Here is an old-ish screenshot that probably is missing things or isn't terribly helpful:

![Screenshot of RRT pathing GUI](https://cdn.discordapp.com/attachments/449903525653250079/839222900488929280/unknown.png)

This repo is under the GPLv3 license as I copied Joshua Barretto's A\* pathfinding code from Veloren for comparison with my RRT paths. I plan on releasing a crate with the algorithms in this repo as well as various RRT\* algorithms under the MIT and Apache licenses eventually.
