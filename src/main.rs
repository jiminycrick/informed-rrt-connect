use kiddo::distance::squared_euclidean;
use kiddo::ErrorKind;
use kiddo::KdTree;
use macroquad::{input::{is_key_down, KeyCode}, prelude::{
    clear_background, draw_circle, draw_line, draw_rectangle, draw_rectangle_lines, draw_text, next_frame, Color,
    Conf, ORANGE, BLACK, BLUE, GREEN, RED, WHITE, PURPLE, YELLOW,
}};
use rand::{prelude::IteratorRandom, thread_rng, Rng, distributions::{Distribution, Uniform}};
use std::{f32::consts::PI, fs, time::Instant};
use vek::{LineSegment2, Mat2, Vec2};
use core::{
    cmp::Ordering::{self, Equal},
    f32, fmt,
    hash::{BuildHasher, Hash},
};
use hashbrown::{hash_map::DefaultHashBuilder, HashSet, HashMap};
use std::collections::BinaryHeap;

//const SCALE_FACTOR: f32 = 15.0;
const SCALE_FACTOR: f32 = 5.0;
const DIRS: [Vec2<i32>; 4] = [
    Vec2::new(0, 1),   // Forward
    Vec2::new(1, 0),   // Right
    Vec2::new(0, -1),  // Backwards
    Vec2::new(-1, 0),  // Left
];
const DIAGONAL_DIRS: [Vec2<i32>; 8] = [
    Vec2::new(0, 1),   // Forward
    Vec2::new(1, 0),   // Right
    Vec2::new(0, -1),  // Backwards
    Vec2::new(-1, 0),  // Left
    Vec2::new(1, -1),
    Vec2::new(1, 1),
    Vec2::new(-1, 1),
    Vec2::new(-1, -1),
];

fn window_conf() -> Conf {
    Conf {
        window_title: "Pathfinding".to_owned(),
        window_width: 940,
        window_height: 580,
        ..Default::default()
    }
}

#[derive(Default, Debug)]
struct Timings {
    astar: f32,
    forward: f32,
    reverse: f32,
    rrt: f32,
    rrt_opt: f32,
    rrt_connect: f32,
}


impl Timings {
    fn add(&self, other: &Timings) -> Timings {
        Self {
            astar: self.astar + other.astar,
            forward: self.forward + other.forward,
            reverse: self.reverse + other.reverse,
            rrt: self.rrt + other.rrt,
            rrt_opt: self.rrt_opt + other.rrt_opt,
            rrt_connect: self.rrt_connect + other.rrt_connect,
        }
    }

    fn div(&self, factor: f32) -> Timings {
        Self {
            astar: self.astar / factor,
            forward: self.forward / factor,
            reverse: self.reverse / factor,
            rrt: self.rrt / factor,
            rrt_opt: self.rrt_opt / factor,
            rrt_connect: self.rrt_connect / factor,
        }
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut map = Map::read("map.txt");

    let mut astar_path = Some(Vec::new());
    //let mut forward_path = Vec::new();
    //let mut reverse_path = Vec::new();
    //let mut rrt = Ok((Vec::new(), Vec::new()));
    //let mut rrt_opt = Ok((Vec::new(), Vec::new()));
    let mut rrt_connect = Ok((Vec::new(), Vec::new()));

    let mut timings = Vec::new();

    let mut show_paths = false;

    loop {
        clear_background(BLACK);
        if is_key_down(KeyCode::Space) {
            show_paths = true;

            let astar_time = Instant::now();
            astar_path = map.astar();
            let astar_duration = astar_time.elapsed().as_secs_f32();
            println!("A* time: {}", astar_duration);

            let forward_time = Instant::now();
            //forward_path = map.forward_path(1000);
            let forward_duration = forward_time.elapsed().as_secs_f32();

            let reverse_time = Instant::now();
            //reverse_path = map.reverse_path();
            let reverse_duration = reverse_time.elapsed().as_secs_f32();

            let rrt_time = Instant::now();
            //rrt = map.rrt();
            let rrt_duration = rrt_time.elapsed().as_secs_f32();

            let rrt_opt_time = Instant::now();
            //rrt_opt = map.rrt_opt();
            let rrt_opt_duration = rrt_opt_time.elapsed().as_secs_f32();

            let rrt_connect_time = Instant::now();
            rrt_connect = map.rrt_connect();
            let rrt_connect_duration = rrt_connect_time.elapsed().as_secs_f32();
            println!("RRT connect time: {}", rrt_connect_duration);
            println!("\n");

            let durations = Timings {
                astar: astar_duration,
                forward: forward_duration,
                reverse: reverse_duration,
                rrt: rrt_duration,
                rrt_opt: rrt_opt_duration,
                rrt_connect: rrt_connect_duration,
            };
            timings.push(durations);
        } else if is_key_down(KeyCode::R) {
            show_paths = false;
            println!("\n\nTime Avgs Reset\n\n");
            timings = Vec::new();
        } else if is_key_down(KeyCode::G) {
            show_paths = false;
            map = Map::generate();
        } else if is_key_down(KeyCode::M) {
            show_paths = false;
            map = Map::read("map.txt");
        } else if is_key_down(KeyCode::S) {
            show_paths = false;
            map.start();
        } else if is_key_down(KeyCode::E) {
            show_paths = false;
            map.end();
        } else if is_key_down(KeyCode::X) {
            map.save();
        }

        let start = map.start.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        let end = map.end.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        for pos in map.blocks.iter() {
            let pos = pos.map(|a| a + 1);
            //draw_rectangle(screen_width() / 2.0 - 60.0, 100.0, 120.0, 60.0, GREEN);
            //draw_rectangle_lines(
            //    SCALE_FACTOR * (pos.x as f32 - 0.5),
            //    SCALE_FACTOR * (pos.y as f32 - 0.5),
            //    SCALE_FACTOR,
            //    SCALE_FACTOR,
            //    1.0,
            //    GREEN,
            //);
            draw_rectangle(
                SCALE_FACTOR * (pos.x as f32 - 0.5),
                SCALE_FACTOR * (pos.y as f32 - 0.5),
                SCALE_FACTOR,
                SCALE_FACTOR,
                WHITE,
            );
        }
        draw_circle(start.x, start.y, 4.0, RED);
        draw_circle(end.x, end.y, 4.0, YELLOW);

        let num_trials = timings.len() as f32;
        let line_start = 501.0;
        if show_paths {
            if num_trials > 0.0 {
                let totals = timings.iter().map(|x| x.div(num_trials)).fold(Timings::default(), |acc, x| acc.add(&x));
                draw_text(
                    &format!("RRT connect avg time: {}s", totals.rrt_connect),
                    400.0,
                    line_start + 15.0,
                    20.0,
                    ORANGE,
                );
                draw_text(
                    &format!("A* avg time: {}s", totals.astar),
                    400.0,
                    line_start + 30.0,
                    20.0,
                    PURPLE,
                );
            }
            let draw_graph_path = |path: &Vec<Vec2<i32>>, color: Color| {
                let path_len = path.len();
                for (i, pos) in path.iter().enumerate() {
                    let pos1 = pos.map(|a| (a + 1) as f32 * SCALE_FACTOR);
                    let pos2_option = if i < path_len - 1 {
                        Some(path[i + 1].map(|a| (a + 1) as f32 * SCALE_FACTOR))
                    } else {
                        None
                    };
                    if let Some(pos2) = pos2_option {
                        draw_line(pos1.x, pos1.y, pos2.x, pos2.y, 1.0, color);
                    }
                    draw_circle(pos1.x, pos1.y, 2.0, color);
                }
            };
            let draw_path = |path: &Vec<Vec2<f32>>, color: Color| {
                let path_len = path.len();
                for (i, pos1) in path.iter().enumerate() {
                    let pos1 = pos1.map(|a| a + 1.0);
                    let pos2_option = if i < path_len - 1 {
                        Some(path[i + 1].map(|a| a + 1.0))
                    } else {
                        None
                    };
                    if let Some(pos2) = pos2_option {
                        draw_line(pos1.x, pos1.y, pos2.x, pos2.y, 1.0, color);
                    }
                    let size = 2.0;
                    draw_circle(pos1.x, pos1.y, size, color);
                }
            };
            //draw_graph_path(&forward_path, YELLOW);
            //draw_graph_path(&reverse_path, GREEN);
            //draw_text(
            //    &format!("Forward path length: {}", forward_path.len()),
            //    0.0,
            //    SCALE_FACTOR * (line_start + 1.0),
            //    20.0,
            //    WHITE,
            //);
            //draw_text(
            //    &format!("Reverse path length: {}", reverse_path.len()),
            //    0.0,
            //    SCALE_FACTOR * (line_start + 2.0),
            //    20.0,
            //    WHITE,
            //);
            //match &rrt {
            //    Ok(rrt_path) => {
            //        //for (pos1, pos2) in rrt_path.1.iter() {
            //        //    draw_line(pos1.x, pos1.y, pos2.x, pos2.y, 1.0, YELLOW);
            //        //}
            //        //draw_path(&rrt_path.0, RED);
            //        let mut length = 0.0;
            //        let num_nodes = rrt_path.0.len();
            //        for (i, pos) in rrt_path.0.iter().enumerate() {
            //            if i+1 < num_nodes {
            //                length += rrt_path.0[i].distance(rrt_path.0[i+1]);
            //            }
            //        }
            //        draw_text(
            //            &format!("RRT path length: {}", length / SCALE_FACTOR),
            //            0.0,
            //            SCALE_FACTOR * (line_start + 3.0),
            //            20.0,
            //            RED,
            //        );
            //    },
            //    Err(e) => draw_text(&format!("{:?}", e), 0.0, SCALE_FACTOR * (line_start + 3.0), 20.0, WHITE),
            //}

            //match &rrt_opt {
            //    Ok(rrt_path) => {
            //        //for (pos1, pos2) in rrt_path.1.iter() {
            //        //    draw_line(pos1.x, pos1.y, pos2.x, pos2.y, 1.0, YELLOW);
            //        //}
            //        //draw_path(&rrt_path.0, ORANGE);
            //        let mut length = 0.0;
            //        let num_nodes = rrt_path.0.len();
            //        for (i, pos) in rrt_path.0.iter().enumerate() {
            //            if i+1 < num_nodes {
            //                length += rrt_path.0[i].distance(rrt_path.0[i+1]);
            //            }
            //        }
            //        draw_text(
            //            &format!("RRT opt path length: {}", length / SCALE_FACTOR),
            //            0.0,
            //            SCALE_FACTOR * (line_start + 4.0),
            //            20.0,
            //            ORANGE,
            //        );
            //    },
            //    Err(e) => draw_text(&format!("{:?}", e), 0.0, SCALE_FACTOR * (line_start + 5.0), 20.0, WHITE),
            //}
            match &rrt_connect {
                Ok(rrt_path) => {
                    for (pos1, pos2) in rrt_path.1.iter() {
                        draw_line(pos1.x, pos1.y, pos2.x, pos2.y, 1.0, GREEN);
                    }
                    draw_path(&rrt_path.0, ORANGE);
                    let mut length = 0.0;
                    let num_nodes = rrt_path.0.len();
                    for (i, pos) in rrt_path.0.iter().enumerate() {
                        if i+1 < num_nodes {
                            length += rrt_path.0[i].distance(rrt_path.0[i+1]);
                        }
                    }
                    draw_text(
                        &format!("RRT connect path length: {}", length / SCALE_FACTOR),
                        0.0,
                        line_start + 15.0,
                        20.0,
                        ORANGE,
                    );
                },
                Err(e) => draw_text(&format!("{:?}", e), 0.0, SCALE_FACTOR * (line_start + 6.0), 20.0, WHITE),
            }
            if let Some(path) = &astar_path {
                draw_graph_path(&path, PURPLE);
                draw_text(
                    &format!("A* path length: {}", path.len() - 1),
                    0.0,
                    line_start + 30.0,
                    20.0,
                    PURPLE,
                );
            }
        }


        next_frame().await
    }
}

pub struct Map {
    /// Basic stationary platform positions
    pub blocks: Vec<Vec2<i32>>,
    pub start: Vec2<i32>,
    pub end: Vec2<i32>,
}

impl Map {
    pub fn read(filepath: &str) -> Self {
        let mut blocks = Vec::new();
        let mut start = Vec2::new(0, 0);
        let mut end = Vec2::new(70, 17);
        let contents = fs::read_to_string(filepath).unwrap();
        for (line_index, line) in contents.lines().enumerate() {
            let chars: Vec<char> = line.chars().collect();
            for (char_index, c) in chars.iter().enumerate() {
                if Some(*c) == "#".chars().next() {
                    blocks.push(Vec2::new(char_index as i32, line_index as i32));
                } else if Some(*c) == "0".chars().next() {
                    start = Vec2::new(char_index as i32, line_index as i32);
                } else if Some(*c) == "1".chars().next() {
                    end = Vec2::new(char_index as i32, line_index as i32);
                }
            }
        }

        Self { blocks, start, end }
    }

    fn save(&self) {
        let mut file = std::fs::File::create("map.txt").expect("create failed");
        let mut map_string = "".to_owned();
        for i in 0..100 {
            let mut row = vec![0; 200];
            self.blocks.iter().filter(|pos| pos.y == i).for_each(|pos| row[pos.x as usize] = 1);
            for (j, cell) in row.iter().enumerate() {
                if self.start.y == i as i32 && self.start.x == j as i32 {
                    map_string.push_str("0");
                } else if self.end.y == i as i32 && self.end.x == j as i32 {
                    map_string.push_str("1");
                } else if *cell == 0 {
                    map_string.push_str(" ");
                } else {
                    map_string.push_str("#");
                }
            }
            map_string.push_str("\n");
        }
        fs::write("map.txt", map_string.as_bytes()).expect("write failed");
    }

    fn start(&mut self) {
        let mut rng = thread_rng();
        let mut new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        while self.blocks.contains(&new_pos) {
            new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        }
        self.start = new_pos;
    }

    fn end(&mut self) {
        let mut rng = thread_rng();
        let mut new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        while self.blocks.contains(&new_pos) {
            new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        }
        self.end = new_pos;
    }

    fn generate() -> Self {
        let mut rng = thread_rng();
        let mut blocks = Vec::new();
        let mut start = Vec2::new(0, 0);
        let mut end = Vec2::new(70, 17);
        let mut pick_start = false;
        let mut pick_end = false;
        for x in 0..200 {
            for y in 0..100 {
                let pos = Vec2::new(x, y);
                if rng.gen_bool(0.60) {
                    blocks.push(pos);
                }
            }
        }
        for _i in 0..6 {
            let mut next_blocks = Vec::new();
            for x in 0..200 {
                for y in 0..100 {
                    let pos = Vec2::new(x, y);
                    if DIAGONAL_DIRS.iter().map(|dir| *dir + pos).filter(|b| blocks.contains(b)).count() > 4 {
                        next_blocks.push(pos);
                    }
                }
            }
            blocks = next_blocks;
        }
        let mut new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        while blocks.contains(&new_pos) {
            new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        }
        start = new_pos;

        let mut new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        while blocks.contains(&new_pos) {
            new_pos = Vec2::new(rng.gen_range(5..195), rng.gen_range(5..95));
        }
        end = new_pos;

        Self { blocks, start, end }
    }

    fn rrt(&self) -> Result<(Vec<Vec2<f32>>, Vec<(Vec2<f32>, Vec2<f32>)>), ErrorKind> {
        let radius: f32 = 14.0;
        let mut node_index: usize = 0;
        let mut nodes = Vec::new();
        let mut parents = HashMap::new();
        let mut path = Vec::new();
        let mut kdtree = KdTree::new();
        let start = self.start.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        let end = self.end.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        let search_min = start.x.min(end.x).min(start.y.min(end.y)) - 70.0;
        let search_max = start.x.max(end.x).max(start.y.max(end.y)) + 70.0;
        kdtree.add(&[start.x, start.y], node_index)?;
        nodes.push(start);
        node_index += 1;
        let mut new_point = start;
        let mut rng = thread_rng();
        while new_point.distance_squared(end) > radius.powi(2) && node_index < 2000 {
            let point = 
                Vec2::new(
                    rng.gen_range(search_min..search_max),
                    rng.gen_range(search_min..search_max),
                );
            let nearest_index = *kdtree.nearest_one(&[point.x, point.y], &squared_euclidean)?.1 as usize;
            let nearest = nodes[nearest_index];
            new_point = nearest
                + (point - nearest)
                    .try_normalized()
                    .unwrap()
                    .map(|a| a * radius);
            if !self.intersects_block(&new_point) {
                kdtree.add(&[new_point.x, new_point.y], node_index)?;
                nodes.push(new_point);
                parents.insert(node_index, nearest_index);
                node_index += 1;
            }
        }
        let nearest_index = *kdtree.nearest_one(&[end.x, end.y], &squared_euclidean)?.1 as usize;
        kdtree.add(&[end.x, end.y], node_index)?;
        nodes.push(end);
        parents.insert(node_index, nearest_index);
        path.push(end);
        let mut current_node_index = node_index;
        while current_node_index > 0 {
            current_node_index = *parents.get(&current_node_index).unwrap();
            path.push(nodes[current_node_index]);
        }
        path.reverse();

        let branches = parents.iter().map(|(a, b)| (nodes[*a], nodes[*b])).collect::<Vec<(Vec2<f32>, Vec2<f32>)>>();
        Ok((path, branches))
    }

    fn rrt_connect(&self) -> Result<(Vec<Vec2<f32>>, Vec<(Vec2<f32>, Vec2<f32>)>), ErrorKind> {
        let radius = SCALE_FACTOR - 1.0;
        let start = self.start.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        let end = self.end.map(|a| (a + 1) as f32 * SCALE_FACTOR);

        let mut node_index1: usize = 0;
        let mut node_index2: usize = 0;

        let mut nodes1 = Vec::new();
        let mut parents1 = HashMap::new();
        let mut path1 = Vec::new();
        let mut kdtree1 = KdTree::new();
        kdtree1.add(&[start.x, start.y], node_index1)?;
        nodes1.push(start);
        node_index1 += 1;

        let mut nodes2 = Vec::new();
        let mut parents2 = HashMap::new();
        let mut path2 = Vec::new();
        let mut kdtree2 = KdTree::new();
        kdtree2.add(&[end.x, end.y], node_index2)?;
        nodes2.push(end);
        node_index2 += 1;

        let mut connect = false;
        let mut connection1_idx = 0;
        let mut connection2_idx = 0;

        let mut search_parameter = 1.0;

        let mut iterations = 0;
        while !connect && iterations < 7000 {
            iterations += 1;
            let (sampled_point1, sampled_point2) = {//if thread_rng().gen_bool(0.25) {
            //    (end, start)
            //} else {
                let point = ellipse_point(start, end, search_parameter);
                (point, point)
            };

            let nearest_index1 = *kdtree1.nearest_one(&[sampled_point1.x, sampled_point1.y], &squared_euclidean)?.1 as usize;
            let nearest_index2 = *kdtree2.nearest_one(&[sampled_point2.x, sampled_point2.y], &squared_euclidean)?.1 as usize;

            let nearest1 = nodes1[nearest_index1];
            let nearest2 = nodes2[nearest_index2];
            let new_point1 = nearest1
                + (sampled_point1 - nearest1)
                    .try_normalized()
                    .unwrap()
                    .map(|a| a * radius);
            let new_point2 = nearest2
                + (sampled_point2 - nearest2)
                    .try_normalized()
                    .unwrap()
                    .map(|a| a * radius);

            let intersect1 = self.intersects_block(&new_point1);
            if !intersect1 {
                kdtree1.add(&[new_point1.x, new_point1.y], node_index1)?;
                nodes1.push(new_point1);
                parents1.insert(node_index1, nearest_index1);
                node_index1 += 1;
                let (check, index) = kdtree2.nearest_one(&[new_point1.x, new_point1.y], &squared_euclidean)?;
                if check < radius {
                    let connection = nodes2[*index];
                    connection2_idx = *index;
                    nodes1.push(connection);
                    connection1_idx = nodes1.len() - 1;
                    parents1.insert(node_index1, node_index1 - 1);
                    connect = true;
                }
            }

            let intersect2 = self.intersects_block(&new_point2);
            if !intersect2 {
                kdtree2.add(&[new_point2.x, new_point2.y], node_index2)?;
                nodes2.push(new_point2);
                parents2.insert(node_index2, nearest_index2);
                node_index2 += 1;
                let (check, index) = kdtree1.nearest_one(&[new_point2.x, new_point2.y], &squared_euclidean)?;
                if check < radius {
                    let connection = nodes1[*index];
                    connection1_idx = *index;
                    nodes2.push(connection);
                    connection2_idx = nodes2.len() - 1;
                    parents2.insert(node_index2, node_index2 - 1);
                    connect = true;
                }
            }
            search_parameter += 0.2;
        }
        let mut path = Vec::new();
        if connect {
            let mut current_node_index1 = connection1_idx;
            while current_node_index1 > 0 {
                current_node_index1 = *parents1.get(&current_node_index1).unwrap();
                path1.push(nodes1[current_node_index1]);
            }
            let mut current_node_index2 = connection2_idx;
            while current_node_index2 > 0 {
                current_node_index2 = *parents2.get(&current_node_index2).unwrap();
                path2.push(nodes2[current_node_index2]);
            }
            path1.reverse();
            path.append(&mut path1);
            path.append(&mut path2);
            path.dedup();
        } else {
            let mut current_node_index1 = kdtree1.nearest_one(&[end.x, end.y], &squared_euclidean)?.1;
            for _i in 0..3 {
                if *current_node_index1 == 0 || nodes1[*current_node_index1].distance_squared(start) < 4.0 {
                    current_node_index1 = parents1.values().choose(&mut thread_rng()).unwrap();
                } else {
                    break;
                }
            }
            path1.push(nodes1[*current_node_index1]);
            while *current_node_index1 != 0 && nodes1[*current_node_index1].distance_squared(start) > 4.0 {
                current_node_index1 = parents1.get(&current_node_index1).unwrap();
                path1.push(nodes1[*current_node_index1]);
            }

            path1.reverse();
            path.append(&mut path1);
        }

        let mut path_len = path.len();
        let close_radius = (0.5 * SCALE_FACTOR).powi(2);

        for _j in 0..2 {
            for i in 0..path_len {
                if i + 4 < path_len {
                    let new_point = 0.5 * (path[i] + path[i + 4]);
                    if !self.intersects_block(&new_point) {
                        path[i + 2] = new_point;
                    }
                    let new_point = 0.5 * (path[i] + path[i + 2]);
                    if !self.intersects_block(&new_point) {
                        path[i + 1] = new_point;
                    }
                } else if i + 2 < path_len {
                    let new_point = 0.5 * (path[i] + path[i + 2]);
                    if !self.intersects_block(&new_point) {
                        path[i + 1] = new_point;
                    }
                }
            }
        }

        if path.len() == 0 {
            println!("connect? {}", connect);
        }
        let mut branches = parents1.iter().map(|(a, b)| (nodes1[*a], nodes1[*b])).collect::<Vec<(Vec2<f32>, Vec2<f32>)>>();
        branches.append(&mut parents2.iter().map(|(a, b)| (nodes2[*a], nodes2[*b])).collect::<Vec<(Vec2<f32>, Vec2<f32>)>>());
        Ok((path, branches))
    }

    fn rrt_opt(&self) -> Result<(Vec<Vec2<f32>>, Vec<(Vec2<f32>, Vec2<f32>)>), ErrorKind> {
        let radius: f32 = 14.0;
        let mut node_index: usize = 0;
        let mut nodes = Vec::new();
        let mut parents = HashMap::new();
        let mut path = Vec::new();
        let mut kdtree = KdTree::new();
        let start = self.start.map(|a| (a + 1) as f32 * SCALE_FACTOR);
        let end = self.end.map(|a| (a + 1) as f32 * SCALE_FACTOR);

        let forward_path = self.forward_path(1000);

        let (search_x_min, search_y_min, search_x_max, search_y_max) = if forward_path.contains(&self.end) {
            ((forward_path.iter().map(|pos| pos.x).min().unwrap() - 3) as f32 * SCALE_FACTOR,
            (forward_path.iter().map(|pos| pos.y).min().unwrap() - 3) as f32 * SCALE_FACTOR,
            (forward_path.iter().map(|pos| pos.x).max().unwrap() + 3) as f32 * SCALE_FACTOR,
            (forward_path.iter().map(|pos| pos.y).max().unwrap() + 3) as f32 * SCALE_FACTOR)
        } else {
            (start.x.min(end.x) - 50.0, start.y.min(end.y) - 50.0, start.x.max(end.x) + 50.0, start.y.max(end.y) + 50.0)
        };
        let c = end.distance(start);
        let b = search_y_max.max(search_x_max) - search_y_min.max(search_x_min);
        let s = 60.0;//(c.powi(2) + b.powi(2)).powf(0.5) - c;
        kdtree.add(&[start.x, start.y], node_index)?;
        nodes.push(start);
        node_index += 1;
        let mut new_point = start;
        let mut rng = thread_rng();
        while new_point.distance_squared(end) > radius.powi(2) && node_index < 2000 {
            let point = ellipse_point(start, end, s);
                //Vec2::new(
                //    rng.gen_range(search_x_min.max(0.0)..search_x_max),
                //    rng.gen_range(search_y_min.max(0.0)..search_y_max),
                //);
            let nearest_index = *kdtree.nearest_one(&[point.x, point.y], &squared_euclidean)?.1 as usize;
            let nearest = nodes[nearest_index];
            new_point = nearest
                + (point - nearest)
                    .try_normalized()
                    .unwrap()
                    .map(|a| a * radius);
            if !self.intersects_block(&new_point) {
                kdtree.add(&[new_point.x, new_point.y], node_index)?;
                nodes.push(new_point);
                parents.insert(node_index, nearest_index);
                node_index += 1;
            }
        }
        let nearest_index = *kdtree.nearest_one(&[end.x, end.y], &squared_euclidean)?.1 as usize;
        kdtree.add(&[end.x, end.y], node_index)?;
        nodes.push(end);
        parents.insert(node_index, nearest_index);
        path.push(end);
        let mut current_node_index = node_index;
        while current_node_index > 0 {
            current_node_index = *parents.get(&current_node_index).unwrap();
            path.push(nodes[current_node_index]);
        }
        path.reverse();

        let branches = parents.iter().map(|(a, b)| (nodes[*a], nodes[*b])).collect::<Vec<(Vec2<f32>, Vec2<f32>)>>();
        Ok((path, branches))
    }

    fn intersects_block(&self, point: &Vec2<f32>) -> bool {
        self.blocks.contains(&point.map(|a| (a / SCALE_FACTOR).round() as i32 - 1))
    }

    fn forward_path(&self, max_iterations: u32) -> Vec<Vec2<i32>> {
        let mut path = Vec::new();
        let start = self.start;
        let end = self.end;
        path.push(start);
        let mut pos = start;
        let mut iteration = 0;
        while pos != end && iteration < max_iterations {
            let mut neighbors = self.neighbors(pos)
                .filter(|n| !path.contains(n));
            if let Some(cheapest_neighbor) = neighbors.next() {
                let mut cheapest_neighbor = *cheapest_neighbor;
                for node in neighbors {
                    let node_cost = node.distance_squared(end);
                    let cheapest_neighbor_cost = cheapest_neighbor.distance_squared(end);
                    if node_cost < cheapest_neighbor_cost {
                        cheapest_neighbor = *node;
                    }
                }

                path.push(cheapest_neighbor);
                pos = cheapest_neighbor;
            } else {
                break;
            }
            iteration += 1;
        }
        path
    }

    fn reverse_path(&self) -> Vec<Vec2<i32>> {
        let mut path = Vec::new();
        let start = self.start;
        let end = self.end;
        path.push(end);
        let mut pos = end;
        let mut iteration = 0;
        while pos != start  && iteration < 1000 {
            let mut neighbors = self.neighbors(pos)
                .filter(|n| !path.contains(n));
            if let Some(cheapest_neighbor) = neighbors.next() {
                let mut cheapest_neighbor = *cheapest_neighbor;
                for node in neighbors {
                    let node_cost = node.distance_squared(start);
                    let cheapest_neighbor_cost = cheapest_neighbor.distance_squared(start);
                    if node_cost < cheapest_neighbor_cost {
                        cheapest_neighbor = *node;
                    }
                }

                path.push(cheapest_neighbor);
                pos = cheapest_neighbor;
            } else {
                break;
            }
            iteration += 1;
        }
        path
    }

    fn astar(&self) -> Option<Vec<Vec2<i32>>> {
        let start = self.start;
        let end = self.end;
        let heuristic = |pos: &Vec2<i32>| (pos.distance_squared(end) as f32).sqrt();
        let neighbors = |pos: &Vec2<i32>| {
            let pos = *pos;

            DIRS.iter()
                .map(move |dir| (pos, dir))
                .filter(move |(pos, dir)| {
                    !self.blocks.contains(pos) && !self.blocks.contains(&(*pos + **dir))
                })
                .map(move |(pos, dir)| pos + dir)
        };

        let transition = |a: &Vec2<i32>, b: &Vec2<i32>| {
            let startf = self.start.map(|a| (a + 1) as f32);
            let endf = self.end.map(|a| (a + 1) as f32);
            let crow_line = LineSegment2 {
                start: startf,
                end: endf,
            };

            // Modify the heuristic a little in order to prefer paths that take us on a
            // straight line toward our target. This means we get smoother movement.
            1.0 + crow_line.distance_to_point(b.map(|e| e as f32)) * 0.025
        };
        let satisfied = |pos: &Vec2<i32>| pos == &end;

        let mut new_astar = Astar::new(25_000, start, heuristic, DefaultHashBuilder::default());

        //let path_result = new_astar.poll(1000, heuristic, neighbors, transition, satisfied);
        let path_result = new_astar.poll(2000, heuristic, neighbors, transition, satisfied);

        match path_result {
            PathResult::Path(path) => {
                Some(path)
            },
            PathResult::None(path) => {
                Some(path)
            },
            PathResult::Exhausted(path) => {
                Some(path)
            },
            PathResult::Pending => None
        }
    }

    pub fn neighbors(&self, pos: Vec2<i32>) -> impl Iterator<Item = &Vec2<i32>> {
        DIAGONAL_DIRS.iter()
            .filter(move |dir| {
                !self.blocks.contains(&(pos + *dir))
            })
    }
}

pub fn ellipse_point(foci1: Vec2<f32>, foci2: Vec2<f32>, search_parameter: f32) -> Vec2<f32> {
    let mut rng = thread_rng();
    let range = Uniform::from(0.0..1.0);
    let midpoint = 0.5 * (foci1 + foci2);
    let c: f32 = 0.5 * foci1.distance(foci2);
    let a: f32 = c + search_parameter;
    let b: f32 = (a.powi(2) - c.powi(2)).powf(0.5);
    let width = foci2.x - foci1.x;
    let height = foci2.y - foci1.y;
    let phi: f32 = if width != 0.0 {
        (height / width).atan()
    } else {
        0.5 * PI
    };
    let theta: f32 = 2.0 * PI * range.sample(&mut rng);
    let k: f32 = range.sample(&mut rng).powf(0.5);
    let xy = Vec2::new(a * k * theta.cos(), b * k * theta.sin());
    //let rot_mat = Mat2::new(phi.cos(), (0.5 * PI + phi).cos(), (0.5 * PI - phi).cos(), phi.cos());
    let rot_mat = Mat2::new(phi.cos(), -1.0 * phi.sin(), phi.sin(), phi.cos());
    //let rot_mat = Mat2::new(phi.sin(), phi.cos(), -1.0 * phi.sin(), );
    let xy_rotated = midpoint + rot_mat * xy;
    xy_rotated
}

#[derive(Copy, Clone, Debug)]
pub struct PathEntry<S> {
    cost: f32,
    node: S,
}

impl<S: Eq> PartialEq for PathEntry<S> {
    fn eq(&self, other: &PathEntry<S>) -> bool { self.node.eq(&other.node) }
}

impl<S: Eq> Eq for PathEntry<S> {}

impl<S: Eq> Ord for PathEntry<S> {
    // This method implements reverse ordering, so that the lowest cost
    // will be ordered first
    fn cmp(&self, other: &PathEntry<S>) -> Ordering {
        other.cost.partial_cmp(&self.cost).unwrap_or(Equal)
    }
}

impl<S: Eq> PartialOrd for PathEntry<S> {
    fn partial_cmp(&self, other: &PathEntry<S>) -> Option<Ordering> { Some(self.cmp(other)) }
}

pub enum PathResult<T> {
    None(Vec<T>),
    Exhausted(Vec<T>),
    Path(Vec<T>),
    Pending,
}

impl<T> PathResult<T> {
    pub fn into_path(self) -> Option<Vec<T>> {
        match self {
            PathResult::Path(path) => Some(path),
            _ => None,
        }
    }
}

#[derive(Clone)]
pub struct Astar<S, Hasher> {
    iter: usize,
    max_iters: usize,
    potential_nodes: BinaryHeap<PathEntry<S>>,
    came_from: HashMap<S, S, Hasher>,
    cheapest_scores: HashMap<S, f32, Hasher>,
    final_scores: HashMap<S, f32, Hasher>,
    visited: HashSet<S, Hasher>,
    cheapest_node: Option<S>,
    cheapest_cost: Option<f32>,
}

/// NOTE: Must manually derive since Hasher doesn't implement it.
impl<S: Clone + Eq + Hash + fmt::Debug, H: BuildHasher> fmt::Debug for Astar<S, H> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Astar")
            .field("iter", &self.iter)
            .field("max_iters", &self.max_iters)
            .field("potential_nodes", &self.potential_nodes)
            .field("came_from", &self.came_from)
            .field("cheapest_scores", &self.cheapest_scores)
            .field("final_scores", &self.final_scores)
            .field("visited", &self.visited)
            .field("cheapest_node", &self.cheapest_node)
            .field("cheapest_cost", &self.cheapest_cost)
            .finish()
    }
}

impl<S: Clone + Eq + Hash, H: BuildHasher + Clone> Astar<S, H> {
    pub fn new(max_iters: usize, start: S, heuristic: impl FnOnce(&S) -> f32, hasher: H) -> Self {
        Self {
            max_iters,
            iter: 0,
            potential_nodes: core::iter::once(PathEntry {
                cost: 0.0,
                node: start.clone(),
            })
            .collect(),
            came_from: HashMap::with_hasher(hasher.clone()),
            cheapest_scores: {
                let mut h = HashMap::with_capacity_and_hasher(1, hasher.clone());
                h.extend(core::iter::once((start.clone(), 0.0)));
                h
            },
            final_scores: {
                let mut h = HashMap::with_capacity_and_hasher(1, hasher.clone());
                h.extend(core::iter::once((start.clone(), heuristic(&start))));
                h
            },
            visited: {
                let mut s = HashSet::with_capacity_and_hasher(1, hasher);
                s.extend(core::iter::once(start));
                s
            },
            cheapest_node: None,
            cheapest_cost: None,
        }
    }

    pub fn poll<I>(
        &mut self,
        iters: usize,
        mut heuristic: impl FnMut(&S) -> f32,
        mut neighbors: impl FnMut(&S) -> I,
        mut transition: impl FnMut(&S, &S) -> f32,
        mut satisfied: impl FnMut(&S) -> bool,
    ) -> PathResult<S>
    where
        I: Iterator<Item = S>,
    {
        let iter_limit = self.max_iters.min(self.iter + iters);
        while self.iter < iter_limit {
            if let Some(PathEntry { node, .. }) = self.potential_nodes.pop() {
                if satisfied(&node) {
                    return PathResult::Path(self.reconstruct_path_to(node));
                } else {
                    for neighbor in neighbors(&node) {
                        let node_cheapest = self.cheapest_scores.get(&node).unwrap_or(&f32::MAX);
                        let neighbor_cheapest =
                            self.cheapest_scores.get(&neighbor).unwrap_or(&f32::MAX);

                        let cost = node_cheapest + transition(&node, &neighbor);
                        if cost < *neighbor_cheapest {
                            self.came_from.insert(neighbor.clone(), node.clone());
                            self.cheapest_scores.insert(neighbor.clone(), cost);
                            let h = heuristic(&neighbor);
                            let neighbor_cost = cost + h;
                            self.final_scores.insert(neighbor.clone(), neighbor_cost);

                            if self.cheapest_cost.map(|cc| h < cc).unwrap_or(true) {
                                self.cheapest_node = Some(node.clone());
                                self.cheapest_cost = Some(h);
                            };

                            if self.visited.insert(neighbor.clone()) {
                                self.potential_nodes.push(PathEntry {
                                    node: neighbor,
                                    cost: neighbor_cost,
                                });
                            }
                        }
                    }
                }
            } else {
                return PathResult::None(
                    self.cheapest_node
                        .clone()
                        .map(|lc| self.reconstruct_path_to(lc))
                        .unwrap_or_default(),
                );
            }

            self.iter += 1
        }

        if self.iter >= self.max_iters {
            PathResult::Exhausted(
                self.cheapest_node
                    .clone()
                    .map(|lc| self.reconstruct_path_to(lc))
                    .unwrap_or_default(),
            )
        } else {
            PathResult::Pending
        }
    }

    pub fn get_cheapest_cost(&self) -> Option<f32> { self.cheapest_cost }

    fn reconstruct_path_to(&mut self, end: S) -> Vec<S> {
        let mut path = vec![end.clone()];
        let mut cnode = &end;
        while let Some(node) = self.came_from.get(cnode) {
            path.push(node.clone());
            cnode = node;
        }
        path.into_iter().rev().collect()
    }
}
